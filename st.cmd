# Require Modules
require iocshutils
require essioc
require s7plc
require modbus
require calc

# Update menuScan (1/14 second)
addScan 0.0714





#### 

# Register IOC db directory
epicsEnvSet(DBDIR, "$(E3_CMD_TOP)/db/")

# Load standard module startup scripts


iocshLoad("$(essioc_DIR)/common_config.iocsh")


# Standard PLC Snippet coming from PLC Factory
iocshLoad("iocsh/cwm_cws03_ctrl_plc_01.iocsh")

# AUX Records
dbLoadRecords("db/aux_records.db")



# LLRF Records 
iocshLoad("iocsh/llrf_rfq_control_link.iocsh")





